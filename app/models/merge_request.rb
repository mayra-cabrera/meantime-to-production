class MergeRequest < ApplicationRecord
  enum status: [:opened, :closed, :merged]

  scope :by_month, ->(start_month, end_month) { where(created_at_gitlab: start_month..end_month) }
  scope :merged, -> { where.not(merged_at: nil) }
  scope :by_created_at, -> { order(:created_at_gitlab) }
  scope :by_merged_at, -> { order(:merged_at) }
  scope :on_production, -> { where.not(production_at: nil) }

  def self.average_mttp(merge_requests)
    time = 0

    merge_requests.each do |merge_request|
      time += merge_request.time_between_merged_and_production
    end

    time.to_f / merge_requests.count.to_f
  end

  def self.average_mttp_with_development(merge_requests)
    time = 0

    merge_requests.each do |merge_request|
      time += merge_request.time_between_opened_and_production
    end

    time.to_f / merge_requests.count.to_f
  end

  def self.merged_csv
    attributes = %w{gitlab_path created_at_gitlab merged_at staging_at canary_at production_at}

    CSV.generate(headers: true) do |csv|
      csv << attributes

      merged.by_merged_at.each do |merge_request|
        csv << attributes.map{ |attr| merge_request.send(attr) }
      end
    end
  end

  def self.mttp_with_development_csv
    attributes = %w{gitlab_path time_between_opened_and_production time_between_development_and_merged time_between_merged_and_staging time_between_staging_and_canary time_between_canary_and_production}

    CSV.generate(headers: true) do |csv|
      csv << attributes

      merged.by_merged_at.each do |merge_request|
        csv << attributes.map{ |attr| merge_request.send(attr) }
      end
    end
  end

  def self.mttp_csv
    attributes = %w{gitlab_path time_between_merged_and_production time_between_merged_and_staging time_between_staging_and_canary time_between_canary_and_production}

    CSV.generate(headers: true) do |csv|
      csv << attributes

      merged.by_merged_at.each do |merge_request|
        csv << attributes.map{ |attr| merge_request.send(attr) }
      end
    end
  end

  def gitlab_path
    "https://gitlab.com/gitlab-org/gitlab/merge_requests/#{gitlab_id}"
  end

  def time_between_development_and_merged
    (merged_at.to_date - created_at_gitlab.to_date).to_i
  end

  def time_between_merged_and_staging
    return 0 if staging_at.nil?

    (staging_at.to_date - merged_at.to_date).to_i
  end

  def time_between_staging_and_canary
    return 0 if staging_at.nil? || canary_at.nil?

    (canary_at.to_date - staging_at.to_date).to_i
  end

  def time_between_canary_and_production
    return 0 if canary_at.nil? || production_at.nil?

    (production_at.to_date - canary_at.to_date).to_i
  end

  def time_between_merged_and_production
    return 0 if production_at.nil?

    (production_at.to_date - merged_at.to_date).to_i
  end

  def time_between_opened_and_production
    return 0 if production_at.nil?

    (production_at.to_date - created_at_gitlab.to_date).to_i
  end
end
