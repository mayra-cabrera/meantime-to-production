class StatisticsController < ApplicationController
  def index
    @september = "01-09-2019".to_date
    @september_mrs_total = MergeRequest
      .by_month(@september.beginning_of_month, @september.end_of_month)
      .count
  end
end
