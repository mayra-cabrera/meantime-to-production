class MergeRequestsController < ApplicationController
  before_action :set_date

  def by_month
    @merge_requests = MergeRequest
      .by_month(@date.beginning_of_month, @date.end_of_month)

    respond_to do |format|
      format.html
      format.csv do
        send_data MergeRequest.merged_csv, filename: "merge_requests-merged-#{@date.to_s}.csv" 
      end
    end
  end

  def mttp_with_development
    @merge_requests = MergeRequest
      .merged
      .by_month(@date.beginning_of_month, @date.end_of_month)

    respond_to do |format|
      format.html
      format.csv do
        send_data MergeRequest.mttp_with_development_csv, filename: "mtt-with-development-#{@date.to_s}.csv" 
      end
    end
  end

  def mttp
    @merge_requests = MergeRequest
      .merged
      .by_month(@date.beginning_of_month, @date.end_of_month)

    respond_to do |format|
      format.html
      format.csv do
        send_data MergeRequest.mttp_csv, filename: "mttp-#{@date.to_s}.csv" 
      end
    end
  end

  private

  def set_date
    @date = params[:date].to_date
  end
end
