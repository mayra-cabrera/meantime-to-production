class CreateMergeRequests < ActiveRecord::Migration[5.2]
  def change
    create_table :merge_requests do |t|
      t.string :gitlab_id, null: false
      t.integer :status
      t.timestamp :created_at_gitlab
      t.timestamp :merged_at
      t.timestamp :staging_at
      t.timestamp :canary_at
      t.timestamp :production_at
    end
  end
end
