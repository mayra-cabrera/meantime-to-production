Rails.application.routes.draw do
  root to: 'statistics#index'

  get 'statistics/', to: 'statistics#index'

  resources :merge_requests do
    get 'by_month', on: :collection
    get 'mttp_with_development', on: :collection
    get 'mttp', on: :collection
  end
end
