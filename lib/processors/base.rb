module Processors
  class Base
    GITLAB_PROJECT_ID = 278_964

    include ::HTTParty
    base_uri 'https://gitlab.com/api/v4/'

    private

    attr_reader :project

    def base_options
      { query: { private_token: token, per_page: 100} }
    end

    def token
      ENV['GITLAB_PAT_TOKEN'] || raise(ArgumentError, '$GITLAB_PAT_TOKEN not set')
    end

    def pages
      (1...20).to_a
    end
  end
end
