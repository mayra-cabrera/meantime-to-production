module Processors
  class MergeRequestManual

    def execute
      # Merge requests on Canary by Sept 11 on 12.3.201909101620-4f50f813a07.1c819536903
      merge_request_ids = [
        15986,
        15995,
        15998,
        16098,
        16120,
        16146,
        16079, 
        16146,
        16217,	
        16028,	
        16205,	
        16230,	
        16228,	
        16248	
      ]

      MergeRequest.where(gitlab_id: merge_request_ids).update_all(production_at: '13-09-2019'.to_datetime)

      # Merge requests on Canary by Sept 11 on 12.3.201909110820-6dee6b5dffb.752e88dbee2
      merge_request_ids = [
        16197,
        16093,
        16033,
        16301,
        16021,
        16318,
        16226,
        16227,
        16070
      ]

      MergeRequest.where(gitlab_id: merge_request_ids).update_all(production_at: '13-09-2019'.to_datetime)

      # Merge requests on Canary by Sept 12 on 12.3.201909121621-a7251e8f45a.50c29b1e3f9 and 12.3.201909122019-a94ae26b915.ea1c86e6143
      merge_request_ids = [
        16512,
        16431,
        16577
      ]

      MergeRequest.where(gitlab_id: merge_request_ids).update_all(production_at: '13-09-2019'.to_datetime)

      # Merge requests merged before than Sept 9
      # when a branch from master was created for auto deploy
      MergeRequest
        .where("merged_at <  ?", '09-09-2019'.to_datetime)
        .where(staging_at: nil)
        .update_all(
          staging_at: '11-09-2019'.to_datetime, # on 12.3.201909110820-6dee6b5dffb.752e88dbee2
          canary_at: '11-09-2019'.to_datetime, # 12.3.201909110820-6dee6b5dffb.752e88dbee2 
          production_at: '11-09-2019'.to_datetime # 12.3.201909110820-6dee6b5dffb.752e88dbee2
      )

      # Merge requests merged before than Sept 17th
      # when a branch from master was created for auto deploy
      MergeRequest
        .where("merged_at <  ?", '17-09-2019'.to_datetime)
        .where(staging_at: nil)
        .update_all(
          staging_at: '17-09-2019'.to_datetime, # on 12.3.201909170421-45702d717cc.ba69e358a86
          canary_at: '17-09-2019'.to_datetime, # 12.3.201909171220-32dae28364b.bd6404053cb 
          production_at: '17-09-2019'.to_datetime # 12.3.201909171220-32dae28364b.bd6404053cb
      )

      # Merge requests merged before than Sept 23th
      MergeRequest
        .where("merged_at <=  ?", '24-09-2019'.to_datetime)
        .where(staging_at: nil)
        .update_all(
          staging_at: '23-09-2019'.to_datetime, # on 12.4.201909231220-a07cf51d7d1.3662ce781bc 
          canary_at: '23-09-2019'.to_datetime # 12.4.201909231220-a07cf51d7d1.3662ce781bc 
      )
    end

    private

    def auto_deploys_on_production
      [
        '12.3.201909031219-e6c0856e5ba.d6a5810eef8', # Sept 3
        '12.3.201909040820-8df560cc438.cfa60ba283a', # Sept 4
        '12.3.201909050420-6bae38369df.3a6912e48aa', # Sept 5
        '12.3.201909052019-0dbc12ee47d.ea8d79c4171', # Sept 6
        '12.3.201909062019-2290c31081a.88ea1e2fbce', # Sept 9
        '12.3.201909101859-23bb95c8a77.964859d40b9', # Sept 10 Security release
        '12.3.201909101859-23bb95c8a77.964859d40b9', # Sept 11
        '12.3.201909110820-6dee6b5dffb.752e88dbee2', # Sept 11
        '12.3.201909130420-513a0f74ada.52ea75bd650', # Sept 13
        '12.3.201909171220-32dae28364b.bd6404053cb', # Sept 17
        '12.3.201909200727-8bdd831ba23.7ea35a0e137' # Sept 20
      ]
    end
  end
end
