module Processors
  class MergeRequests < Base
    GITLAB_BOT_ID = 1_786_152
    WORKFLOW_STAGING_LABEL_ID = 11_838_117
    WORKFLOW_CANARY_LABEL_ID = 11_838_118
    WORKFLOW_PRODUCTION_LABEL_ID = 11_838_120

    def initialize(merge_requests)
      @api_merge_requests = merge_requests
    end

    def execute
      puts "Processing merge requests..."
      api_merge_requests.each do |api_merge_request|
        merge_request = MergeRequest.new
        merge_request.gitlab_id = api_merge_request["iid"]
        merge_request.created_at_gitlab = api_merge_request["created_at"]
        merge_request.status = api_merge_request["state"] unless api_merge_request["state"] == "locked"

        if merge_request.merged?
          merge_request.merged_at = api_merge_request["merged_at"]
          gitlab_bot_label_events = search_for_gitlab_bot_label_events(api_merge_request)

          next if gitlab_bot_label_events.empty?

          merge_request.staging_at = search_for_label_event(gitlab_bot_label_events, WORKFLOW_STAGING_LABEL_ID)
          merge_request.canary_at = search_for_label_event(gitlab_bot_label_events, WORKFLOW_CANARY_LABEL_ID)
          merge_request.production_at = search_for_label_event(gitlab_bot_label_events, WORKFLOW_PRODUCTION_LABEL_ID)
        end

        merge_request.save!
      end
    end

    private

    attr_reader :api_merge_requests

    def search_for_gitlab_bot_label_events(api_merge_request)
      label_events_url =  "/projects/#{GITLAB_PROJECT_ID}/merge_requests/#{api_merge_request["iid"]}/resource_label_events"

      options = base_options
      all_label_events = []

      pages.each do |page|
        options[:query][:page] = page
        result = self.class.get(label_events_url, options)
        break if result.empty?

        all_label_events << result
      end

      all_label_events.flatten!
      all_label_events
        .select { |note| note["user"]["id"] == GITLAB_BOT_ID }
    end

    def search_for_label_event(label_events, label_id)
      label_events
        .select { |label_event| label_event["label"]["id"] == label_id }
        .first&.fetch("created_at", nil)
    end
  end
end
