module Fetchers
  class MergeRequest < Base
    def initialize(date)
      @date = date
      @merge_requests = []
    end

    def execute
      puts "Fetching merge requests created on #{date.strftime("%B")}"

      options = base_options
      raw_merge_requests = []

      pages.each do |page|
        puts "Page: #{page}"
        options[:query][:page] = page
        merge_request_per_page = self.class.get(project_merge_requests_url, options)
        break if merge_request_per_page.empty?

        raw_merge_requests << merge_request_per_page
      end

      raw_merge_requests.flatten!
    end

    private

    attr_reader :date, :merge_requests

    def created_after
      date.beginning_of_month
    end

    def created_before
      date.end_of_month
    end

    def project_merge_requests_url
      "/projects/#{GITLAB_PROJECT_ID}/merge_requests?created_after=#{created_after}&created_before=#{created_before}"
    end
  end
end
