module Trackers
  class MergeRequest
    def self.execute!
      self.process_merge_requests_for("01-09-2019".to_date)
    end

    private

    def self.process_merge_requests_for(date)
      puts "Processing merge requests for #{date.strftime("%B")}"
      
      merge_requests = Fetchers::MergeRequest.new(date).execute
      Processors::MergeRequests.new(merge_requests).execute
    end
  end
end
